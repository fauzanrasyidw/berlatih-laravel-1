@extends('layout.masters')

@section('judulnya')
Halaman Edit Cast {{$cast->id}}
@endsection

@section('contentnya')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label>Nama Pemain</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control"  placeholder="namanya...">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" value="{{$cast->umur}}"class="form-control" placeholder="umurnya...">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" value="{{$cast->bio}}" class="form-control" rows="3"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    <button type="submit "class="btn btn-primary" type="button">Submit</button>
</form>

@endsection