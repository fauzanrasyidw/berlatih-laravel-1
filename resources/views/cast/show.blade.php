@extends('layout.masters')

@section('judulnya')
Halaman Detail Cast
@endsection

@section('contentnya')

<h2>Show Cast {{$cast->id}}</h2>
<h3>{{$cast->nama}}</h3>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

@endsection