@extends('layout.masters')

@section('judulnya')
Halaman List Cast
@endsection

@section('contentnya')

<a href="/cast/create" class="btn btn-success mb-3">Tambah Pemain</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Cast</th>
      <th scope="col">Agenya</th>
      <th scope="col">Bionya</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key=>$value)
    <tr>
        <td>{{$key + 1}}</td>
        <td>{{$value->nama}}</td>
        <td>{{$value->umur}}</td>
        <td>{{$value->bio}}</td>
        <td>
          @auth
          <form action="/cast/{{$value->id}}" method="POST">
            <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            @method('delete')
            @csrf
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
          </form>
          @endauth

          @guest
          <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
          @endguest
        </td>
    </tr>
    @empty
    <tr>
      <td>Data Masih Kosong</td>
    </tr>
    @endforelse
  </tbody>
</table>
@endsection