@extends('layout.masters')

@section('judulnya')
Halaman Form
@endsection

@section('contentnya')

<form action="/cast" method="POST">
  @csrf
  <div class="form-group">
    <label>Nama Pemain</label>
    <input type="text" name="nama" id="nama" class="form-control"  placeholder="namanya...">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" id="umur" class="form-control" placeholder="umurnya...">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" id="bio"  class="form-control" rows="3"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    <button type="submit "class="btn btn-primary" type="button">Submit</button>
</form>

@endsection