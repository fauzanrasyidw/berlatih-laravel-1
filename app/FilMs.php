<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilMs extends Model
{
    protected $table = "films";
    protected $fillable = ["judul", "ringkasan", "poster", "genre_id"];
}
