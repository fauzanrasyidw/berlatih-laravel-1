<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function forms()
    {
        return view('halaman.form');
    }

    public function welcome(request $request)
    {
        $nama1 = $request->nama_depan;
        $nama2 = $request->nama_belakang;


        return view('halaman.welcomenya', compact('nama1', 'nama2'));
    }

    public function tabbles()
    {
        return view('halaman.tables');
    }

    public function datatabless()
    {
        return view('halaman.data-table');
    }
}
